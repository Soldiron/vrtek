#pragma once
#include "package.h"
#include <memory>
#include <boost/property_tree/ptree.hpp>
using namespace std;
using namespace boost::property_tree;

class serializer
{
private:
	const string _rootPackageName = "$";

public:
	serializer();
	~serializer();

	ptree serialize(shared_ptr<package> package);
};

