#include "serializer.h"
#include "package.h"


serializer::serializer()
{
}


serializer::~serializer()
{
}


ptree serializer::serialize(shared_ptr<package> serializablePackage)
{
	ptree root;
	for (unsigned int i = 0; i < serializablePackage->getChilds().size(); i++)
	{
		root.add_child(serializablePackage->getChilds()[i]->getName(), serialize(serializablePackage->getChilds()[i]));
	}
	return root;
}



