#pragma once
#include <vector>
#include <memory>
#include <string>
using namespace std;

class package
{
private:
	string _name;
	vector<shared_ptr<package>> _children;

public:
	package(string name);
	~package();
	string getName();
	vector<shared_ptr<package>> getChilds();
	void add(string packageName);
	void remove(string packageName);
	void print();

	shared_ptr<package> getChild(vector<string> path, unsigned int index);
};

