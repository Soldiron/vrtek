#include "package.h"
#include <string>
#include <iostream>
#include "Core.h"

using namespace std;

package::package(string name) : _name(name), _children()
{
	DBOUT(cout << "Package created: " << _name << '\n');
}


package::~package()
{
	DBOUT(cout << "Package destroyed: " << _name << '\n');
}

string package::getName()
{
	return _name;
}

vector<shared_ptr<package>> package::getChilds()
{
	return _children;
}

void package::add(string packageName)
{
	shared_ptr<package> p = make_shared<package>(packageName);
	_children.push_back(p);
}

void package::remove(string packageName)
{
	int childIndex = -1;
	for (unsigned int i = 0; i < _children.size(); i++) {
		if (_children[i]->_name == packageName) {
			childIndex = i;
			break;
		}
	}

	if (childIndex >= 0)
		_children.erase(_children.begin() + childIndex);
	else
		cout << "Remove failed. No package: " << packageName << '\n';
}

void package::print()
{
	cout << _name << '\n';
	for (unsigned int i = 0; i < _children.size(); i++) {
		_children[i]->print();
	}
}

shared_ptr<package> package::getChild(vector<string> path, unsigned int index)
{
	if (index >= path.size())
		return shared_ptr<package>();

	for (unsigned int i = 0; i < _children.size(); i++) {
		if (_children[i]->_name == path[index])
		{
			if (index == path.size() - 1)
				return _children[i];
			else
				return _children[i]->getChild(path, index + 1);
		}
	}

	return shared_ptr<package>();
}
