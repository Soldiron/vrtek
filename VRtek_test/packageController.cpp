#include "packageController.h"
#include <iostream>
#include "utils.h"
#include "Core.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

using namespace boost::property_tree;


packageController::packageController() 
{
	_rootPackage = make_shared<package>(_rootPackageName);
}


packageController::~packageController()
{
}

vector<string> packageController::getPath(string input)
{
	return split(input, '/');
}

void packageController::createPackage(string packageName)
{
	_rootPackage->add(packageName);
}

void packageController::addPackage(string pathStr, string packageName)
{
	vector<string> path = getPath(pathStr);
	shared_ptr<package> package = _rootPackage->getChild(path, 0);
	if (package)
		package->add(packageName);
	else
		cout << "Add packege fail. Path: " << pathStr << "; New package name: " << packageName <<  '\n';
}

void packageController::removePackage(string pathStr)
{
	vector<string> path = getPath(pathStr);
	string packageToRemoveName = path[path.size() - 1];
	path.pop_back();
	shared_ptr<package> package;
	
	if (path.size())
		package = _rootPackage->getChild(path, 0);
	else
		package = _rootPackage;

	if (package)
		package->remove(packageToRemoveName);
	else
		cout << "Remove packege fail. Path: " << pathStr << '\n';
}

void packageController::printPackage(string pathStr)
{
	vector<string> path = getPath(pathStr);
	shared_ptr<package> package = _rootPackage->getChild(path, 0);
	if (package)
		package->print();
	else
		cout << "Print packege fail. Path: " << pathStr << '\n';
}

void packageController::serialize()
{
	ptree ptree = serializer.serialize(_rootPackage);
	write_json(cout, ptree);

}

