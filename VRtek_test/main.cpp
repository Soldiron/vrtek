#include <vector>
#include <iostream>
#include <fstream>
#include "packageController.h"
#include "utils.h"

using namespace std;

enum command
{
	COMMAND_NONE,
	COMMAND_CREATE,
	COMMAND_ADD,
	COMMAND_REMOVE,
	COMMAND_PRINT,
	COMMAND_SERIALIZE,
	COMMAND_EXIT
};


command getCommand(const string& commandStr)
{
	command command;
	if (commandStr == "create")
	{
		command = COMMAND_CREATE;
	}
	else if (commandStr == "add")
	{
		command = COMMAND_ADD;
	}
	else if (commandStr == "remove")
	{
		command = COMMAND_REMOVE;
	}
	else if (commandStr == "print")
	{
		command = COMMAND_PRINT;
	}
	else if (commandStr == "serialize")
	{
		command = COMMAND_SERIALIZE;
	}
	else if (commandStr == "exit")
	{
		command = COMMAND_EXIT;
	}
	else
	{
		command = COMMAND_NONE;
	}

	return command;
}

bool action(packageController& packageController, string input)
{
	vector<string> commandSequence = split(input, ' ');
	if (commandSequence.size() == 0)
		return false;
	command command = getCommand(commandSequence[0]);
	switch (command)
	{
	case COMMAND_CREATE:
	{
		if (commandSequence.size() != 2)
		{
			cout << "Should be 1 param with CREATE command\n";
			break;
		}
		packageController.createPackage(commandSequence[1]);
		break;
	}
	case COMMAND_ADD:
	{
		if (commandSequence.size() != 3)
		{
			cout << "Should be 2 params with ADD command\n";
			break;
		}
		packageController.addPackage(commandSequence[1], commandSequence[2]);
		break;
	}
	case COMMAND_REMOVE:
	{
		if (commandSequence.size() != 2)
		{
			cout << "Should be 1 param with REMOVE command\n";
			break;
		}
		packageController.removePackage(commandSequence[1]);
		break;
	}
	case COMMAND_PRINT:
	{
		if (commandSequence.size() != 2)
		{
			cout << "Should be 1 param with PRINT command\n";
			break;
		}
		packageController.printPackage(commandSequence[1]);
		break;
	}
	case COMMAND_SERIALIZE:
	{
		if (commandSequence.size() != 1)
		{
			cout << "Should be no param with SERIALIZE command\n";
			break;
		}
		packageController.serialize();
		break;
	}
	case COMMAND_EXIT:
	{
		return true;
	}
	case COMMAND_NONE:
	{
		if (commandSequence.size() == 0)
			break;

		cout << "Command " << commandSequence[0] << " not found\n";
		break;
	}
	}
	return false;
}

int main()
{
	cout << "Usage:\n\n";
	cout << "create <package_name>      - creates root package\n";
	cout << "add <path> <package_name>  - adds package with specified name at specified path\n";
	cout << "remove <path>              - removes package at specified path\n";
	cout << "print <path>               - prints package hierarchy at specified path\n";
	cout << "serialize                  - prints all existing packages data in JSON format\n";
	cout << "exit                       - terminates the program\n\n";
	cout << "examples:  create a\n";
	cout << "           add a b\n";
	cout << "           add a/b c\n";
	cout << "           remove a/b\n\n\n";
	packageController packageController;
	
	string commandStr;
	while (1) {
		getline(cin, commandStr);
		if (action(packageController, commandStr))
			break;
	}

	return 0;
}