#include "utils.h"
using namespace std;

vector<string> split(const string &text, char separator)
{
	vector<string> tokens;
	size_t start = 0, end = 0;
	while ((end = text.find(separator, start)) != string::npos)
	{
		if (start - end > 0)
			tokens.push_back(text.substr(start, end - start));
		start = end + 1;
	}
	if (start < text.length())
		tokens.push_back(text.substr(start));
	return tokens;
}

