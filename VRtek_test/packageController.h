#pragma once
#include "package.h"
#include <memory>
#include "serializer.h"

using namespace std;

class packageController
{
private:
	serializer serializer;
	const string _rootPackageName ="$";

	vector<string> getPath(string input);
public:
	shared_ptr<package> _rootPackage;

	packageController();
	~packageController();

	void createPackage(string packageName);
	void addPackage(string path, string packageName);
	void removePackage(string pathStr);
	void printPackage(string pathStr);
	void serialize();
};

